from neural_network import NeuralNetwork
import numpy as np
import matplotlib.pyplot as matplot

# Number of input, hidden and output nodes
input_nodes = 784
hidden_nodes = 250
output_nodes = 10

# The learning rate of the neural network
learning_rate = 0.2

# Create an instance of the neural network
neuralNet = NeuralNetwork(input_nodes, hidden_nodes,
                          output_nodes, learning_rate)

# Load the mnist training data CSV file into a list
trainingdata_file = open("mnist_dataset/mnist_train.csv", 'r')
trainingdata_list = trainingdata_file.readlines()
trainingdata_file.close()

# Train the neural network

# Epochs are the amount of times we train the network
epochs = 5
for epoch in range(epochs):
    # Go through all records in the training data set
    for record in trainingdata_list:
        # Split the record values by the commas
        all_values = record.split(',')
        # Scale and shift the inputs
        inputs = (np.asfarray(all_values[1:]) / 255.0 * 0.99) + 0.01
        # Create the target output values (all 0.01, except the desired label which is 0.99)
        targets = np.zeros(output_nodes) + 0.01
        # all_values[0] is the target label for this record
        targets[int(all_values[0])] = 0.99
        neuralNet.train(inputs, targets)
        pass
    pass
# Load the mnist test data csv file into a list
test_data_file = open("mnist_dataset/mnist_test.csv", 'r')
test_data_list = test_data_file.readlines()
test_data_file.close()

scorecard = []

# Test the neural network
for record in test_data_list:
    test_value = record.split(',')
    # Get the correct answer
    correct_label = int(test_value[0])

    # Scale and shift inputs
    inputs = (np.asfarray(test_value[1:]) / 255.0 * 0.99) + 0.01
    # Query the network
    outputs = neuralNet.query(inputs)
    # Sets the highest value in the array as the label
    label = np.argmax(outputs)
    # Append correct or incorrect to list
    if label == correct_label:
        scorecard.append(1)
    else:
        scorecard.append(0)
        pass
    pass

scorecard_array = np.asarray(scorecard)
print("performance = ", scorecard_array.sum() / scorecard_array.size)
