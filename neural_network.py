import numpy as numpy
import scipy.special as scipy_special


class NeuralNetwork:

    # Initialisation of the neural network
    def __init__(self, input_nodes, hidden_nodes, output_nodes, learning_rate):
        # Sets the amount of nodes used in the neural network for the input, hidden and ouput layers
        self.input_nodes = input_nodes
        self.hidden_nodes = hidden_nodes
        self.output_nodes = output_nodes

        # Link weight matrices
        self.weights_input_hidden = numpy.random.normal(
            0.0, pow(self.hidden_nodes, -0.5), (self.hidden_nodes, self.input_nodes))
        self.weights_hidden_output = numpy.random.normal(
            0.0, pow(self.output_nodes, -0.5), (self.output_nodes, self.hidden_nodes))

        # Sets the learning rate for the neural network
        self.learning_rate = learning_rate

        # Activation function is the sigmoid function
        self.activation_function = lambda x: scipy_special.expit(x)
        pass

    # Train the neural network
    def train(self, inputs_list, targets_list):
        # Convert inputs list and targets list to a 2D array
        inputs = numpy.array(inputs_list, ndmin=2).T
        targets = numpy.array(targets_list, ndmin=2).T

        # Calculate signals into the hidden layer
        hidden_inputs = numpy.dot(self.weights_input_hidden, inputs)
        # Calculate the signals emerging from the hidden layer
        hidden_outputs = self.activation_function(hidden_inputs)

        # Calculate signals into the final output layer
        final_inputs = numpy.dot(self.weights_hidden_output, hidden_outputs)
        # Calculate the signals emerging from the final output layer
        final_outputs = self.activation_function(final_inputs)

        # Calculates the error of the outputs compared to the target
        output_errors = targets - final_outputs

        # Hidden layer error is the dot product of weights of hidden to output and the output errors
        hidden_errors = numpy.dot(self.weights_hidden_output.T, output_errors)

        # Update the weights for the links between the hidden and output layers
        # This is the equivalent of the mathematical expression:
        # alpha * errors_output_node2 * sigmoid(output_node2) * (1 - sigmoid(output_node2)) . output_node1
        self.weights_hidden_output += self.learning_rate * numpy.dot((output_errors * final_outputs *
                                                                      (1.0 - final_outputs)), numpy.transpose(hidden_outputs))

        # Update the weights for the links between the input and hidden layers
        self.weights_input_hidden += self.learning_rate * numpy.dot((hidden_errors * hidden_outputs *
                                                                     (1.0 - hidden_outputs)), numpy.transpose(inputs))
        pass

    # Query the neural network
    def query(self, inputs_list):
        # Convert inputs list to a 2D array
        inputs = numpy.array(inputs_list, ndmin=2).T
        # Calculate signals into the hidden layer
        hidden_inputs = numpy.dot(self.weights_input_hidden, inputs)
        # Calculate the signals emerging from the hidden layer
        hidden_outputs = self.activation_function(hidden_inputs)

        # Calculate signals into the final output layer
        final_inputs = numpy.dot(self.weights_hidden_output, hidden_outputs)
        # Calculate the signals emerging from the final output layer
        final_outputs = self.activation_function(final_inputs)
        return final_outputs
